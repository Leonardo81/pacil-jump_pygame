'''Leonardo'''
'''1806191023'''
'''DDP-1 F'''
import pygame               #import pygame module and random module
import random

class Brick:                               #class Brick
    def __init__(self,x,y, length):                 #init method making variable self.groundx, self.groundy, self.length  
        self.groundx=x                              #then drawing the brick, and add the object to Bricks set
        self.groundy=y
        self.length = length
        self.draw()
        Bricks.add(self)
        
    def draw(self):                                 #draw method for drawing the block, based self.groundx, and self.groundy position
        global screen
        block=pygame.transform.scale(brick,(self.length-30,30))
        screen.blit(block,(self.groundx+30, self.groundy+40))
        

class Character:                            #class Character
    def __init__(self, x,y):                        #init method, making self.x, self.y
        self.x=x    
        self.y=y
        self.t = 10                                 #self.t is variable to count air time
        self.jump =False                            #self.jump, self.cantright, self.cantleft is variable to know the state of charachter
        self.cantleft =False
        self.cantright= False                       #then drawing the charachter
        self.draw()                
        
    def draw(self):                                 #draw method to drawing charachter on the screen, based on self.x, and self.y position
        global screen
        man=pygame.transform.scale(character,(30,40))
        screen.blit(man, (self.x,self.y))

    def checkair(self):                             #checkair method, to checking on air state when the character not landing on brick
        for obj in Bricks:
            if self.y==obj.groundy and self.x<=obj.groundx+obj.length and self.x>=obj.groundx:
                return False
        return True

    def checkground(self):                          #checkground method, to checking if the charachter landing a brick
        for obj in Bricks:
            if self.y>=obj.groundy  and self.y<obj.groundy+70 and self.x<obj.groundx+obj.length and self.x>obj.groundx:
                self.y=obj.groundy
                return True
        return False

    def cant_left(self):                            #cant_left method, to checking if the character hit brick on it's left the it cant move left
        for obj in Bricks:
            if self.x==obj.groundx+obj.length and self.y>obj.groundy  and self.y<obj.groundy+70:
                return True
        return False
    
    def cant_right(self):                           #cant_right method, to checking if the character hit brick on it's right the it cant move right
        for obj in Bricks:
            if self.x==obj.groundx and self.y>obj.groundy and self.y<obj.groundy+70:
                return True
        return False
    
    def hit_top(self):                              #hit_top method to check if the charachter hitting brick on top of it's head or not
        for obj in Bricks:
            if self.y>obj.groundy  and self.y<=obj.groundy+70 and self.x<obj.groundx+obj.length and self.x>obj.groundx:
                self.jump=False
                self.y=obj.groundy+70
                self.t=10
                return True
        return False
    
    def move(self):                                 #move method to control charachter movement      
        lst= pygame.key.get_pressed()
        if lst[pygame.K_LEFT]:                      #when left is pressed self.x decrased so it moving left          
            if not self.cant_left():
                self.x-=2.5
        if lst[pygame.K_RIGHT]:                     #when right is pressed self.x increased so it moving right
            if not self.cant_right():
                self.x+=2.5
        if not self.checkair() and not self.jump:   #if not in the air and not jumping, player can press up button to jump
            if lst[pygame.K_UP]:
                self.t = 10
                self.jump=True
        else:                                       #else (when jumping or on the air)
            if self.jump and not self.hit_top():        #if jumping, it checks if character hitting bricks on top of charachter head or not
                if self.t>1:
                    self.y-=((self.t)**2)*0.1
                    self.t-=0.25
                else:
                    self.t=10
                    self.jump=False
            elif self.checkair():                       #elif (not jumping or charachter's head hitting brick)
                self.y+=((10-self.t)**2)*0.1                #it checks if charachter in air or not, then increasing self.y
                self.t-=0.25                                            #so the character looks like falling
                if self.checkground():                      #then it checks if the character hitting ground(landing on brick) or not
                    self.t=10
                
#define arena function for brick placement
def arena(n,x):             # parameter x is x position where the arena drawing starts, parameter n are randoming when calling
    if n==0:
        Brick(x,300, 100)
        Brick(x+250, 400, 100)
        Brick(x+525, 400, 100)
        Brick(x+450, 265, 100)
        Brick(x+100, 100, 100)
        Brick(x+400, 125, 100)
        for aha in range(3):
            Brick(x+(random.randrange(10,75))*10, (random.randrange(5,40))*10, 100)
    elif n==1:
        Brick(x+200,300, 100)
        Brick(x+30, 390, 100)
        Brick(x+380, 450, 100)
        Brick(x+580, 300, 100)
        Brick(x+350, 210,100)
        Brick(x+670, 100, 100)
        Brick(x+30, 100, 100)
        Brick(x+225, 120, 100)
        Brick(x+460, 90, 100)
    else:
        Brick(x+50, 400, 100)
        Brick(x+250, 300, 100)
        Brick(x+500, 350, 100)
        Brick(x+190, 175, 100)
        Brick(x+440, 100, 100)
        Brick(x+650, 250, 100)
        for aha in range(3):
            Brick(x+(random.randrange(10,75))*10, (random.randrange(5,40))*10, 100)
##    Brick(500, (random.randrange(5,40))*10, 100)
    

def refresh():                          ##refresh function for updating screen after moving
    global clock, screen, score
    screen.blit(pacil, (0,0))
    char.draw()
    for obj in Bricks:
        obj.draw()
    score=text.render(str((pygame.time.get_ticks()//100)-time), True, (0,0,0))
    screen.blit(score, (10,10))

    pygame.display.update()
    clock.tick(60)

def forward():                      ##forward function for decreasing x variable of charachter and bricks
    for obj in Bricks:              ## so looks like our screen moving to the right
        obj.groundx-=1.25
    char.x-=1.25

def Lose():                         ##function Lose to checking if the player has lost or not
    global INGAME, screen, text         #and draw text "Game Over"
    if char.x<-50 or char.y>590:
        INGAME=False
        lose_msg=text.render("Game Over",True,(0,0,0))
        message=pygame.transform.scale(lose_msg,(400, 70))
        screen.blit(message, (150,200))
        pygame.display.update()

##main function        
def main():
    global screen, pacil, clock, Bricks, brick, character, x_arena, char, INGAME, text, time
    pygame.init()
    
    brick=pygame.image.load('img/Bricks.jpg')               #load picture
    character=pygame.image.load('img/char.png')
    background=pygame.image.load('img/Pacil.jpg')
    
    screen = pygame.display.set_mode((720, 540))        #creating background
    pacil=pygame.transform.scale(background,(720,540))      
    screen.blit(pacil, (0,0))

    text=pygame.font.Font(None, 30)
    clock=pygame.time.Clock()                   #create Clock object
    Bricks = set()                              #creating set for brick
    INGAME=True                                 #INGAME variable 'TRUE' means the game is playing
    time = pygame.time.get_ticks()//100         #the time when main() called, (will be used for scoring)
    
    char = Character(170,0)                     #creating Character object
    arena(0,0)                                  #making first arena by calling arena(0,0) function
    x_arena=0

    while True:                             ##mainloop for the video game
        while INGAME:                           ##mainloop while INGAME True
            for obj in Bricks:
                if obj.groundx<-100:
                    Bricks.remove(obj)      ##removing object that passed off the screen
                    break
            if not x_arena%540:
                arena(random.randrange(0,3),720)    #randoming the arena type (the type in the function)
                
            forward()                       #the main event of the game, the screen moving forward, increasing x_arena variable
            x_arena+=1.25                                                   #moving character by user control, and refresh the screen
            char.move()                                                     #then checking if player has lost or not
            refresh()
            Lose()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            elif event.type == pygame.KEYDOWN:              ##if INGAME == False, you can simply call main() to play again
                if event.key== pygame.K_SPACE:                              ##by pressing spacebar
                    main()
    
if __name__ == '__main__':
    main()

''' Reference '''
''' https://www.pygame.org/docs/ '''
''' https://www.youtube.com/playlist?list=PLzMcBGfZo4-lp3jAExUCewBfMx3UZFkh5 (video 1&2)'''
